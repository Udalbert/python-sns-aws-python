******Python SNS Client******

1.- pip install python_sns_aws_client

2.- Add credentials to env or aws-cli
    
    AWS_ACCESS_KEY_ID = 'AWS_KEY'
    AWS_SECRET_ACCESS_KEY = 'AWS_SECRET'
    AWS_DEFAULT_REGION = 'AWS_REGION'
    AWS_ARN = 'AWS_ARN'
    
    Or usage credentials aws-cli
    
    pip install awscli --upgrade --user
    - in folder ~/.aws/config
        [default]
        region = 'your-region'
        output = text
    - in folder ~/.aws/credentials
        [default]
        aws_access_key_id = 'AWS_KEY'
        aws_secret_access_key = 'AWS_SECRET'
    - add in env
        AWS_DEFAULT_REGION = 'AWS_REGION'
        AWS_ARN = 'AWS_ARN'
    
********USAGE********

1.- Add your routes

    Example Django
    
    from sns.view import PushView, SNSView

    urlpatterns = [
        url(r'^push', PushView.as_view()),
        url(r'^sns', SNSView.as_view()),
    ]
    
    Example Flask
    
    @app.route('/sns', methods=['POST'])
    def sns_topic():
    
    @app.route('/publish', methods=['POST'])
    def sns_publish():
    
2.- Add view or function listen
    
    Example Django basic project
    
    from django.views import generic
    from django.views.decorators.csrf import csrf_exempt
    from django.utils.decorators import method_decorator
    from python_sns_aws_client.utils import Client
    
    @method_decorator(csrf_exempt, name='dispatch')
    class SnsBaseView(generic.TemplateView):
        template_name = 'snsclient.html'
    
        def post(self, request, *args, **kwargs):
            context = self.get_context_data(*args, **kwargs)
            return self.render_to_response(context)
        
    class SNSView(SnsBaseView):
    
        def post(self,request,*args,**kwargs):
            Client.validator(request.body)
            return super().post(request, *args, **kwargs)
    
    #Create template snsclient.html
    
    Example Flask
    @app.route('/sns', methods=['POST'])
    def sns_topic():
        if request.method == 'POST':
            client = Client.validator(request.data)
            return 'SNS-TOPIC!! {}'.format(client)

3.- Push your message to SNS

    Example Django basic project
    
    from django.views import generic
    from django.views.decorators.csrf import csrf_exempt
    from django.utils.decorators import method_decorator
    from python_sns_aws_client.utils import Client
    
    @method_decorator(csrf_exempt, name='dispatch')
    class SnsBaseView(generic.TemplateView):
        template_name = 'snsclient.html'
    
    class PushView(SnsBaseView):

        def post(self, request, *args, **kwargs):
            publish = {
                'subject': request.POST.get('subject'),
                'message': request.POST.get('message')
            }
            cli = Client()
            cli.publish(**publish)
            return super().post(request, *args, **kwargs) 
    
    #Create template snsclient.html
    
    
    Example Django for developer backend
    
    from django.http.response import HttpResponse
    
    @method_decorator(csrf_exempt, name='dispatch')
    class SnsPushBackendView(generic.View):
    
        def post(self, request, **kwargs):
            if request.POST.get('subject') is not None and request.POST.get('message') is not None:
                publish = {
                    'subject': request.POST.get('subject'),
                    'message': request.POST.get('message')
                }
                cli = Client()
                if cli.publish(**publish):
                    return HttpResponse(status=200)
    
            return HttpResponse(status=500)
    
    Example Flask
    
    @app.route('/publish', methods=['POST'])
    def sns_publish():
        if request.method == 'POST':
            publish = {
                'subject': request.form['subject'],
                'message': request.form['message']
            }
            cli = Client()
            cli.publish(**publish)
            return 'PUBLISH-SNS-TOPIC!!'