"""
Extrae del archivo env las variables globales
"""

import env

AWS_DEFAULT_REGION = env.AWS_DEFAULT_REGION
AWS_ARN = env.AWS_ARN

try:
    AWS_ACCESS_KEY_ID = env.AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY = env.AWS_SECRET_ACCESS_KEY
except Exception as e:
    pass
